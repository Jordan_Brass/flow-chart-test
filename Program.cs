﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Please Enter A (Numerical Digit)");
            int A = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please Enter B (Numerical Digit)");
            int B = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please Enter C (Numerical Digit)");
            int C = Convert.ToInt32(Console.ReadLine());
            if (A>B)
            {
                if (A>C)
                {
                    Console.WriteLine(A);
                }
                else
                {
                    Console.WriteLine(B);
                }

            }
            else
            {
                if (B>C)
                {
                    Console.WriteLine(C);
                }
                else
                {
                    Console.WriteLine(B);
                }
            }

        }
    }
}